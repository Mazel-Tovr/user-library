package com.epam.library.exception;

public class DataException extends Exception {
    public DataException(String message) {
        super(message);
    }
}
