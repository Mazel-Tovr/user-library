package com.epam.library.exception;

public class UserException extends Exception
{
    public UserException(String message) {
        super(message);
    }
}
