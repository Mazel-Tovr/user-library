package com.epam.library.userinteraction;

import com.epam.library.businesslayer.LogServiceImpl;
import com.epam.library.businesslayer.ParserEmpowerment;
import com.epam.library.util.parser.IBookParser;
import com.epam.library.businesslayer.interfaces.ILogService;
import com.epam.library.util.parser.CSVParser;
import com.epam.library.util.parser.JsonParser;
import com.epam.library.model.User;

import java.util.Scanner;

public class CSVAndJSONParsersConsole {
    private String DEFAULT_JSON_PATH = "catalog.json";
    private String DEFAULT_CSV_PATH = "catalog.csv";
    private Scanner in = new Scanner(System.in);
    private ILogService logService = new LogServiceImpl();
    private ParserEmpowerment parserEmpowerment = new ParserEmpowerment();
    private User currentUser;

    public CSVAndJSONParsersConsole(User currentUser) {
        this.currentUser = currentUser;
    }

    public void options() {

        String userInput ;
        System.out.println("0 - To go back");
        System.out.println("1 - Json parser");
        System.out.println("2 - CSV parser");
        do  {
            System.out.println("Enter your decision: ");
            userInput = in.nextLine();

            switch (userInput) {

                case "0":break;
                case "1":
                    toParse(new JsonParser());
                    break;
                case "2":
                    toParse(new CSVParser());
                    break;
                default:
                    System.out.println("Unknown command");
            }
            System.out.println("0 - To go back");
            System.out.println("1 - Json parser");
            System.out.println("2 - CSV parser");
        }while (!userInput.toLowerCase().equals("0"));
    }

    private void toParse(IBookParser bookParser) {
        String userInput = "";
        System.out.println("0 - To go back");
        System.out.println("1 - Default path");
        System.out.println("2 - Your path");
        String path = bookParser instanceof JsonParser ? DEFAULT_JSON_PATH : DEFAULT_CSV_PATH;
        while (!(userInput.toLowerCase().equals("0")||userInput.toLowerCase().equals("1")||userInput.toLowerCase().equals("2"))){
            System.out.println("Enter your decision: ");
            userInput = in.nextLine();
            switch (userInput)
            {
                case "1":
                    parserEmpowerment.addBookToDB(bookParser.parseFromFile(path));
                    logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " added book from file");
                    break;
                case "2":
                    System.out.println("Enter a new path");
                    String userPath = in.nextLine();
                    parserEmpowerment.addBookToDB(bookParser.parseFromFile(userPath));
                    logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " added book from file");
                    break;
                default:
                    System.out.println("Unknown command");
            }

        }
    }


}
