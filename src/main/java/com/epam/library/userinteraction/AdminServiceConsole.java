package com.epam.library.userinteraction;

import com.epam.library.businesslayer.LogServiceImpl;
import com.epam.library.businesslayer.SystemExploiter;
import com.epam.library.businesslayer.interfaces.IAdminFeatures;
import com.epam.library.businesslayer.interfaces.ILogService;
import com.epam.library.exception.DataException;
import com.epam.library.exception.UserException;
import com.epam.library.businesslayer.Validation;
import com.epam.library.model.Log;
import com.epam.library.model.User;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class AdminServiceConsole {
    private IAdminFeatures adminFeatures = new SystemExploiter();
    private ILogService logService = new LogServiceImpl();
    private static final Logger logger = Logger.getLogger("InterfaceLayerLogger");
    private Scanner in = new Scanner(System.in);
    private User currentUser;

    public AdminServiceConsole(User currentUser) {
        this.currentUser = currentUser;
    }

    public boolean SignUpAsAdmin() {
        System.out.println("Sign up");
        do {
            String roleId ;
            System.out.println("Enter first name");
            String firstName = in.nextLine();
            System.out.println("Enter last name");
            String lastName = in.nextLine();
            System.out.println("Enter nickname");
            String nickName = in.nextLine();
            System.out.println("Enter password");
            String password = in.nextLine();
            System.out.println("Enter roleId");
            do{
                System.out.println("Enter roleId");
                roleId = in.nextLine();
            } while (!Validation.isPositiveNumber(roleId));
            try {
                adminFeatures.toRegister(firstName, lastName, nickName, password, Integer.parseInt(roleId));
                logService.addLog(currentUser, "Admin: "+currentUser.getNickName()+ " register new user");
                return true;
            } catch (UserException e) {
                logger.info("Registration Error : " + e.getMessage());
            }

            System.out.print("Do you want one more try?");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) return false;
            } while (!userDecision.toLowerCase().equals("y"));

        } while (true);
    }

    public void blockUser() {
        boolean flag = true;
        do {
            String userId ;
             do{
                System.out.println("Enter user id: ");
                userId = in.nextLine();
            }while (!Validation.isPositiveNumber(userId));
            try {
                adminFeatures.blockUser(Integer.parseInt(userId));
                logService.addLog(currentUser, "Admin: "+currentUser.getNickName()+ " block user with id" + userId);
                break;
            } catch (UserException e) {
                logger.info(e.getMessage());
            }
            System.out.print("Do you want one more try?");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    public void getAllLogs()
    {
        try {
            for (Log log:logService.getAllLogs()) {
                System.out.println(log.toString());
            }
            logService.addLog(currentUser, "Admin: "+currentUser.getNickName()+ " got all logs");
        } catch (DataException e) {
           logger.info(e.getMessage());
        }
    }
}
