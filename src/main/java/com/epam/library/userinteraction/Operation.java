package com.epam.library.userinteraction;

import com.epam.library.businesslayer.LogServiceImpl;
import com.epam.library.businesslayer.RoleImpl;
import com.epam.library.businesslayer.interfaces.ILogService;
import com.epam.library.businesslayer.interfaces.IRoleService;
import com.epam.library.exception.DataException;
import com.epam.library.model.Role;
import com.epam.library.model.User;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class Operation {
    private static final Logger logger = Logger.getLogger("InterfaceLayerLogger");

    private IRoleService roleService = new RoleImpl();

    private ILogService logService = new LogServiceImpl();

    private User currentUser;
    private Role userRole;

    public Operation(User currentUser) {
        this.currentUser = currentUser;
        try {
            userRole = roleService.getRole(currentUser.getRole().getRoleId());
        } catch (DataException e) {
            logger.info(e.getMessage());
        }
    }

    public void userAbility() {
        Scanner in = new Scanner(System.in);
        System.out.println("Congratulation you logging in system as " + userRole.getType());
        if (isAdmin()) listOfAdminAbility();
        else listOfUserAbility();

        String userInput ;
        do
        {
            System.out.println("Main");
            System.out.println("Enter your decision: ");
            userInput = in.nextLine();

            switch (userInput) {
                case "0":
                    if (isAdmin()) listOfAdminAbility();
                    else listOfUserAbility();
                    break;
                case "1":
                    new BookServiceConsole(currentUser).addBookToDateBase();
                    break;
                case "2":
                    new BookServiceConsole(currentUser).dropBookById();
                    break;
                case "3":
                    new AuthorServiceConsole(currentUser).addAuthor();
                    break;
                case "4":
                    new AuthorServiceConsole(currentUser).dropAuthorAndAllHisBooks();
                    break;
                case "5":
                    new CSVAndJSONParsersConsole(currentUser).options();
                    break;
                case "6":
                    new BookmarkServiceConsole(currentUser).addBookmark();
                    break;
                case "7":
                    new BookmarkServiceConsole(currentUser).dropBookmark();
                    break;
                case "8":
                    new BookServiceConsole(currentUser).searchBookByPartOfBookName();
                    break;
                case "9":
                    new BookServiceConsole(currentUser).searchBookByPartOfAuthorName();
                    break;
                case "10":
                    new BookServiceConsole(currentUser).searchBookByISBN();
                    break;
                case "11":
                    new BookServiceConsole(currentUser).searchBookByYearRange();
                    break;
                case "12":
                    new BookServiceConsole(currentUser).searchBookByYearAndPageCountAndPartName();
                    break;
                case "13":
                    new BookServiceConsole(currentUser).searchBookWhereUserBookMark();
                    break;
                case "14":
                    if (!isAdmin()) {
                        System.out.println("Unknown command");
                        break;
                    }
                    new AdminServiceConsole(currentUser).SignUpAsAdmin();
                    break;
                case "15":
                    if (!isAdmin()) {
                        System.out.println("Unknown command");
                        break;
                    }
                    new AdminServiceConsole(currentUser).blockUser();
                    break;
                case "16":
                    if (!isAdmin()) {
                        System.out.println("Unknown command");
                        break;
                    }
                    new AdminServiceConsole(currentUser).getAllLogs();
                    break;

                case "!exit":break;
                default:
                    System.out.println("Unknown command");
            }

        }while (!userInput.toLowerCase().equals("!exit"));


    }

    private void listOfUserAbility() {
        System.out.println("Here is list of ur ability in system");
        System.out.println("Write !exit to exit");
        System.out.println("0 - Show all command again");
        System.out.println("1 - You can add book to lib");
        System.out.println("2 - You can delete book from lib");
        System.out.println("3 - You can add author to lib");
        System.out.println("4 - You can delete author and all his book");
        System.out.println("5 - You can add book from CSV,JSON");
        System.out.println("6 - You can add bookmark to book");
        System.out.println("7 - You can delete bookmark from book");
        System.out.println("8 - You can search book by part of name");
        System.out.println("9 - You can search book by part of author name");
        System.out.println("10 - You can search book by ISBN id");
        System.out.println("11 - You can search book by range of years");
        System.out.println("12 - You can search book by year, page count and part of name ");
        System.out.println("13 - You can see all book where you have bookmark");
    }

    private void listOfAdminAbility() {
        listOfUserAbility();
        System.out.println("14 - You can create new user");
        System.out.println("15 - You can block user");
        System.out.println("16 - You can see history of all user actions in the system");
    }

    private boolean isAdmin() {
        return userRole.getType().equals("Admin");
    }

}
