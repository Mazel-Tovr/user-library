package com.epam.library.userinteraction;

import com.epam.library.businesslayer.AuthorServiceImpl;
import com.epam.library.businesslayer.LogServiceImpl;
import com.epam.library.businesslayer.interfaces.IAuthorService;
import com.epam.library.businesslayer.interfaces.ILogService;
import com.epam.library.exception.DataException;
import com.epam.library.businesslayer.Validation;
import com.epam.library.model.User;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class AuthorServiceConsole
{
    private static final Logger logger = Logger.getLogger("InterfaceLayerLogger");
    private IAuthorService authorService = new AuthorServiceImpl();
    private Scanner in = new Scanner(System.in);
    private ILogService logService = new LogServiceImpl();
    private User currentUser;

    public AuthorServiceConsole(User currentUser) {
        this.currentUser = currentUser;
    }

    public void addAuthor()
    {
        boolean flag = true;
        do {

            System.out.println("Enter name: ");
            String name = in.nextLine();
            System.out.println("Enter second name: ");
            String secondName = in.nextLine();
            System.out.println("Enter last name: ");
            String lastName = in.nextLine();
            System.out.println("Enter author dob: ");
            String dob = in.nextLine();
            try {
                authorService.addAuthor(name,secondName,lastName,dob);
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " add new author");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            System.out.print("Do you want one more try?");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    public void dropAuthorAndAllHisBooks()
    {
        boolean flag = true;

        do {
            String authorId;
            do{
                System.out.println("Enter author id: ");
                authorId = in.nextLine();
            } while (!Validation.isPositiveNumber(authorId));
            try {
                authorService.dropAuthorAndAllHisBooks(Integer.parseInt(authorId));
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " drop author");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            System.out.print("Do you want one more try?");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

}
