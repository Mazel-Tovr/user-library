package com.epam.library.userinteraction;

import com.epam.library.businesslayer.SystemExploiter;
import com.epam.library.businesslayer.interfaces.IUserFeatures;
import com.epam.library.exception.UserException;
import com.epam.library.model.User;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class Entrance {

    private IUserFeatures userFeatures = new SystemExploiter();

    private static final Logger logger = Logger.getLogger("InterfaceLayerLogger");

    public boolean SignUpAsUser() {
        System.out.println("Sign up");
        do {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter first name");
            String firstName = in.nextLine();
            System.out.println("Enter last name");
            String lastName = in.nextLine();
            System.out.println("Enter nickname");
            String nickName = in.nextLine();
            System.out.println("Enter password");
            String password = in.nextLine();
            try {
                userFeatures.toRegister(firstName, lastName, nickName, password);
                return true;
            } catch (UserException e) {
                logger.info("Registration Error : " + e.getMessage());
            }

            System.out.print("Do you want one more try?");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) return false;
            } while (!userDecision.toLowerCase().equals("y"));

        } while (true);

    }

    public User SignIn() {
        System.out.println("Sign in");
        do {
            Scanner in = new Scanner(System.in);
            System.out.println("Enter nickname");
            String nickName = in.nextLine();
            System.out.println("Enter password");
            String password = in.nextLine();
            try {
                return userFeatures.login(nickName, password);
            } catch (UserException e) {
                logger.info("Logging Error : " + e.getMessage());
            }

            System.out.print("Do you want one more try?");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) return null;
            } while (!userDecision.toLowerCase().equals("y"));

        } while (true);
    }

}
