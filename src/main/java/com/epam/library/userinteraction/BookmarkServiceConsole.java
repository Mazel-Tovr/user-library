package com.epam.library.userinteraction;

import com.epam.library.businesslayer.BookmarkImpl;
import com.epam.library.businesslayer.LogServiceImpl;
import com.epam.library.businesslayer.interfaces.IBookmarkService;
import com.epam.library.businesslayer.interfaces.ILogService;
import com.epam.library.exception.DataException;
import com.epam.library.businesslayer.Validation;
import com.epam.library.model.User;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class BookmarkServiceConsole {
    private static final Logger logger = Logger.getLogger("InterfaceLayerLogger");
    private IBookmarkService bookmarkService = new BookmarkImpl();
    private ILogService logService = new LogServiceImpl();
    private User currentUser;

    public BookmarkServiceConsole(User currentUser) {
        this.currentUser = currentUser;
    }

    private Scanner in = new Scanner(System.in);

    public void addBookmark() {
        boolean flag = true;
        do {
            String bookId;
            String pageNumber;
            do {
                System.out.println("Enter book id: ");
                bookId = in.nextLine();
            } while (!Validation.isPositiveNumber(bookId));
            do {
                System.out.println("Enter page number: ");
                pageNumber = in.nextLine();
            } while (!Validation.isPositiveNumber(pageNumber));
            try {
                bookmarkService.addBookmark(currentUser.getUserId(), Long.parseLong(bookId), Integer.parseInt(pageNumber));
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " add bookmark");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            System.out.print("Do you want one more try?\n");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    public void dropBookmark() {
        boolean flag = true;
        do {
            String bookmarkId;
            do {
                System.out.println("Enter bookmark id: ");
                bookmarkId = in.nextLine();
            } while (!Validation.isPositiveNumber(bookmarkId));

            try {
                bookmarkService.dropBookmark(Long.parseLong(bookmarkId));
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " drop bookmark");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            System.out.print("Do you want one more try?");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);

    }
}
