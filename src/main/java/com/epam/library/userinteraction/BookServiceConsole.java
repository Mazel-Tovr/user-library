package com.epam.library.userinteraction;

import com.epam.library.businesslayer.BookServiceImpl;
import com.epam.library.businesslayer.LogServiceImpl;
import com.epam.library.businesslayer.interfaces.IBookService;
import com.epam.library.businesslayer.interfaces.ILogService;
import com.epam.library.exception.DataException;
import com.epam.library.businesslayer.Validation;
import com.epam.library.model.Book;
import com.epam.library.model.User;
import org.apache.log4j.Logger;

import java.util.Scanner;


public class BookServiceConsole {
    private static final Logger logger = Logger.getLogger("InterfaceLayerLogger");
    private IBookService bookService = new BookServiceImpl();
    private Scanner in = new Scanner(System.in);
    private ILogService logService = new LogServiceImpl();
    private User currentUser;



    public BookServiceConsole(User currentUser) {
        this.currentUser = currentUser;
    }

    public void addBookToDateBase() {
        boolean flag = true;
        do {
            String releaseYear;
            String pageCount;
            String authorId;
            System.out.println("Enter book name: ");
            String bookName = in.nextLine();
            do {
                System.out.println("Enter release year: ");
                releaseYear = in.nextLine();
            } while (!Validation.isPositiveNumber(releaseYear));
            do {
                System.out.println("Enter page count: ");
                pageCount = in.nextLine();
            } while (!Validation.isPositiveNumber(pageCount));
            System.out.println("Enter ISBN: ");
            String ISBN = in.nextLine();
            System.out.println("Enter publisher ");
            String publisher = in.nextLine();
            do {
                System.out.println("Enter author id: ");
                authorId = in.nextLine();
            } while (!Validation.isPositiveNumber(authorId));

            try {
                bookService.addBookToDateBase(bookName, Integer.parseInt(releaseYear), Integer.parseInt(pageCount),
                        ISBN, publisher, Integer.parseInt(authorId));
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " add new book");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            System.out.print("Do you want one more try?\n");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    void getBook() {
        boolean flag = true;
        do {
            String id;
            do {
                System.out.println("Enter book id");
                id = in.nextLine();
            } while (!Validation.isPositiveNumber(id));
            try {
                System.out.println(bookService.getBook(Integer.parseInt(id)).toString());
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " get book");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            System.out.print("Do you want one more try?\n");
            String userDecision;
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    void getAll() {
        try {
            for (Book book : bookService.getAll()) {
                System.out.println(book.toString());
            }
            logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " get all books");
        } catch (DataException e) {
            logger.info(e.getMessage());
        }
    }

    void dropBookById() {
        boolean flag = true;
        do {
            String id;
            do {
                System.out.println("Enter book id");
                id = in.nextLine();
            } while (!Validation.isPositiveNumber(id));
            try {
                bookService.dropBookById(Integer.parseInt(id));
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " drop book with id "+ id);
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            String userDecision;
            System.out.print("Do you want one more try?\n");
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    void searchBookByPartOfBookName() {
        boolean flag = true;
        do {
            System.out.println("Enter book name");
            String partOfName = in.nextLine();
            try {
                for (Book book : bookService.searchBookByPartOfBookName(partOfName)) {
                    System.out.println(book.toString());
                }
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ "was searching book by part of name");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            String userDecision;
            System.out.print("Do you want one more try?\n");
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    void searchBookByPartOfAuthorName() {
        boolean flag = true;
        do {
            System.out.println("Enter author name");
            String partOfName = in.nextLine();
            try {
                for (Book book : bookService.searchBookByPartOfAuthorName(partOfName)) {
                    System.out.println(book.toString());
                }
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " was searching book by part of author name");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            String userDecision;
            System.out.print("Do you want one more try?\n");
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    void searchBookByISBN() {
        boolean flag = true;
        do {
            System.out.println("Enter book ISBN");
            String ISBN = in.nextLine();
            try {
                System.out.println(bookService.searchBookByISBN(ISBN).toString());
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " was searching book by ISBN");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            String userDecision;
            System.out.print("Do you want one more try?\n");
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    void searchBookByYearRange() {

        boolean flag = true;
        do {

            String from;
            String to;
            do {
                System.out.println("Enter year from");
                from = in.nextLine();
            } while (!Validation.isPositiveNumber(from));
            do {
                System.out.println("Enter year to");
                to = in.nextLine();
            } while (!Validation.isPositiveNumber(to));

            try {
                for (Book book : bookService.searchBookByYearRange(Integer.parseInt(from), Integer.parseInt(to))) {
                    System.out.println(book.toString());
                }
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " searching book by range of years ");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            String userDecision;
            System.out.print("Do you want one more try?\n");
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);

    }

    void searchBookByYearAndPageCountAndPartName() {
        boolean flag = true;
        do {
            String year;
            String pageCount;
            do {
                System.out.println("Enter year ");
                year = in.nextLine();
            } while (!Validation.isPositiveNumber(year));
            do {
                System.out.println("Enter page count");
                pageCount = in.nextLine();
            } while (!Validation.isPositiveNumber(pageCount));
            System.out.println("Enter part of name");
            String partOfName = in.nextLine();
            try {
                for (Book book : bookService.searchBookByYearAndPageCountAndPartName(Integer.parseInt(year),
                        Integer.parseInt(pageCount), partOfName)) {
                    System.out.println(book.toString());
                }
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " was searching books by year,page count and part name");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            String userDecision;
            System.out.print("Do you want one more try?\n");
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

    void searchBookWhereUserBookMark() {
        boolean flag = true;
        do {
            try {
                for (Book book : bookService.searchBookWhereUserBookMark(currentUser.getUserId())) {
                    System.out.println(book.toString());
                }
                logService.addLog(currentUser, "User: "+currentUser.getNickName()+ " was searching books by bookmark");
                break;
            } catch (DataException e) {
                logger.info(e.getMessage());
            }
            String userDecision;
            System.out.print("Do you want one more try?\n");
            do {
                System.out.println("Y/N: ");
                userDecision = in.nextLine();
                if (userDecision.toLowerCase().equals("n")) {
                    flag = false;
                    break;
                }
            } while (!userDecision.toLowerCase().equals("y"));

        } while (flag);
    }

}
