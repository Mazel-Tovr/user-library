package com.epam.library.businesslayer;

import com.epam.library.businesslayer.interfaces.IRoleService;
import com.epam.library.dao.RoleAccesses;
import com.epam.library.dao.interfaces.ICommonOperations;
import com.epam.library.exception.DataException;
import com.epam.library.model.Role;
import org.apache.log4j.Logger;

public class RoleImpl implements IRoleService
{

    private ICommonOperations<Role> roleTable = new RoleAccesses();
    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");

    @Override
    public Role getRole(int roleId) throws DataException {
        Role role =  roleTable.getEntity(roleId);
        if(role == null) throw new DataException("Role with such id doesn't exist");
        return role;
    }

    @Override
    public void addRole(String type) {

        roleTable.addNewEntity(new Role(type));
    }

    @Override
    public void dropRole(int roleId) throws DataException {
       roleTable.deleteEntity(getRole(roleId));
    }
}
