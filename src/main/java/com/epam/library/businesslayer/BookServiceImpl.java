package com.epam.library.businesslayer;


import com.epam.library.businesslayer.interfaces.IBookService;
import com.epam.library.dao.AuthorAccesses;
import com.epam.library.dao.BookAccesses;
import com.epam.library.dao.UserAccesses;
import com.epam.library.dao.interfaces.IAuthorOperations;
import com.epam.library.dao.interfaces.IBookOperations;
import com.epam.library.dao.interfaces.IUserOperation;
import com.epam.library.exception.DataException;
import com.epam.library.model.Author;
import com.epam.library.model.Book;
import org.apache.log4j.Logger;


import java.time.LocalDate;
import java.util.List;

import java.util.regex.Pattern;

public class BookServiceImpl implements IBookService {

    private static final Pattern ISBN_PATTERN = Pattern.compile("\\d{3}\\-\\d{10}");
    private static final Pattern YEAR_PATTERN = Pattern.compile("\\d{1,4}");
    private static final int CURRENT_YEAR = LocalDate.now().getYear();
    private IBookOperations bookTable = new BookAccesses();
    private IAuthorOperations authorTable = new AuthorAccesses();
    private IUserOperation userTable = new UserAccesses();
    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");

    @Override
    public void addBookToDateBase(String bookName, int releaseYear, int pageCount,
                                  String ISBN, String publisher, long authorId) throws DataException {

        if (!YEAR_PATTERN.matcher(String.valueOf(releaseYear)).matches() || releaseYear > CURRENT_YEAR)
            throw new DataException("Date input error");
        if(pageCount < 0)
            throw new DataException("Page count should be more then 0");
        if (!ISBN_PATTERN.matcher(ISBN).matches())
            throw new DataException("ISBN input error");//TODO NEED TEST THIS
        Author author = authorTable.getEntity(authorId);
        if (author == null)
            throw new DataException("Author with such id doesn't exist");
        bookTable.addNewEntity(new Book(bookName, releaseYear, pageCount, ISBN, publisher, author));
    }

    @Override
    public Book getBook(long bookId) throws DataException {
        Book book = bookTable.getEntity(bookId);
        if (book == null)
            throw new DataException("Book with such id does't exist");
        return book;
    }

    @Override
    public List<Book> getAll() throws DataException {
        List<Book> bookList = bookTable.getAll();
        if (bookList.isEmpty())
            throw new DataException("Books are absent");
        return bookList;
    }

    @Override
    public void dropBookById(long bookId) throws DataException {
        bookTable.deleteEntity(getBook(bookId));
    }


    @Override
    public List<Book> searchBookByPartOfBookName(String partOfName) throws DataException {
        List<Book> bookList = bookTable.searchBookByPartOfBookName(partOfName);
        if (bookList.isEmpty())
            throw new DataException("No matches found");
        return bookList;
    }

    @Override
    public List<Book> searchBookByPartOfAuthorName(String partOfAuthorName) throws DataException {
        List<Book> bookList = bookTable.searchBookByPartOfAuthorName(partOfAuthorName);
        if (bookList.isEmpty())
            throw new DataException("No matches found");
        return bookList;
    }

    @Override
    public Book searchBookByISBN(String ISBN) throws DataException {
        Book book = bookTable.searchBookByISBN(ISBN);
        if (book == null)
            throw new DataException("No matches found");
        return book;
    }

    @Override
    public List<Book> searchBookByYearRange(int from, int to) throws DataException {
        if ((!YEAR_PATTERN.matcher(String.valueOf(from)).matches()|| from > CURRENT_YEAR) ||
                (!YEAR_PATTERN.matcher(String.valueOf(to)).matches() || to > CURRENT_YEAR))
                throw new DataException("Date input error");
        List<Book> bookList = bookTable.searchBookByYearRange(from,to);
        if (bookList.isEmpty())
            throw new DataException("No matches found");
        return bookList;
    }

    @Override
    public List<Book> searchBookByYearAndPageCountAndPartName(int year, int pageCount, String partOfName) throws DataException {
        if (!YEAR_PATTERN.matcher(String.valueOf(year)).matches() || year > CURRENT_YEAR)
            throw new DataException("Date input error");
        if(pageCount < 0)
            throw new DataException("Page count should be more then 0");
        List<Book> bookList = bookTable.searchBookByYearAndPageCountAndPartName(year,pageCount,partOfName);
        if (bookList.isEmpty())
            throw new DataException("No matches found");
        return bookList;
    }

    @Override
    public List<Book> searchBookWhereUserBookMark(long userId) throws DataException {
        if(userTable.getEntity(userId) == null)
            throw new DataException("User with such id doesn't exist");
        List<Book> bookList = bookTable.searchBookWhereUserBookMark(userId);
        if (bookList.isEmpty())
            throw new DataException("No matches found");
        return bookList;
    }





}
