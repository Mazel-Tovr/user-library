package com.epam.library.businesslayer;

import com.epam.library.businesslayer.interfaces.IAuthorService;
import com.epam.library.dao.AuthorAccesses;
import com.epam.library.dao.interfaces.IAuthorOperations;
import com.epam.library.exception.DataException;
import com.epam.library.model.Author;
import org.apache.log4j.Logger;


import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;

public class AuthorServiceImpl implements IAuthorService {

    private IAuthorOperations authorTable = new AuthorAccesses();

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");
    private static final Pattern DATE_PATTERN = Pattern.compile("^\\s*((?:19|20)\\d{2})\\-(1[012]|0?[1-9])\\-(3[01]|[12][0-9]|0?[1-9])\\s*$");

    @Override
    public Author getAuthor(int authorId) throws DataException {
        Author author =  authorTable.getEntity(authorId);
        if(author == null) throw new DataException("Author with such id doesn't exist");
        return author;
    }

    @Override
    public void addAuthor(String name, String secondName, String lastName, String dob) throws DataException {
        String  date = dob;
        if(!DATE_PATTERN.matcher(dob).matches()) {
            try {
                date =  new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd.mm.yyyy").parse(dob));
            } catch (ParseException e) {
                throw new DataException("Wrong date format");//TODO tryParse to normal format (if left some time (ofc no))
            }
        }
        authorTable.addNewEntity(new Author(name,secondName,lastName,Date.valueOf(date)));

    }

    @Override
    public void dropAuthorAndAllHisBooks(int authorId) throws DataException {

     Author author =  authorTable.getEntity(authorId);
     if(author == null) throw new DataException("Author with such id doesn't exist");
     authorTable.deleteAuthorAndAllHisBooks(authorId);
    }

    @Override
    public Author getByNameSecondNameLastNameDob(String name, String secondName, String lastName, String dob) throws DataException {
        String date = dob;
        if(!DATE_PATTERN.matcher(dob).matches()) {
            try {
                //Switch to normalFormat
                date =  new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd.mm.yyyy").parse(dob));

            } catch (ParseException e) {
                throw new DataException("Wrong date format");
            }
        }
        return authorTable.isAuthorExist(new Author(name,secondName,lastName,Date.valueOf(date)));
    }
}
