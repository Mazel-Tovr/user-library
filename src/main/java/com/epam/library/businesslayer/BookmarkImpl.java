package com.epam.library.businesslayer;

import com.epam.library.businesslayer.interfaces.IBookmarkService;
import com.epam.library.dao.BookAccesses;
import com.epam.library.dao.BookMarksAccesses;
import com.epam.library.dao.UserAccesses;
import com.epam.library.dao.interfaces.IBookOperations;
import com.epam.library.dao.interfaces.ICommonOperations;
import com.epam.library.dao.interfaces.IUserOperation;
import com.epam.library.exception.DataException;
import com.epam.library.model.Bookmark;
import org.apache.log4j.Logger;

public class BookmarkImpl implements IBookmarkService {

    private IBookOperations bookTable = new BookAccesses();
    private IUserOperation userTable = new UserAccesses();
    private ICommonOperations<Bookmark> bookmarkTable = new BookMarksAccesses();
    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");

    @Override
    public void addBookmark(long userId, long bookId, int pageNumber) throws DataException {

        if (userTable.getEntity(userId) == null) throw new DataException("User with such id doesn't exist");
        if (bookTable.getEntity(bookId) == null) throw new DataException("Book with such id doesn't exist");
        if (pageNumber < 0) throw new DataException("Page count should be more then 0");
        if (bookTable.getEntity(bookId).getPageCount() < pageNumber) throw new DataException("Book doesn't have such page");
        logger.debug("Validation successfully passed");
        bookmarkTable.addNewEntity(new Bookmark(userId, bookId, pageNumber));

    }

    @Override
    public Bookmark getBookmark(long bookmarkId) throws DataException {
        Bookmark bookmark = bookmarkTable.getEntity(bookmarkId);
        if (bookmark == null) throw new DataException("Bookmark with such id doesn't exist");
        return bookmark;
    }

    @Override
    public void dropBookmark(long bookmarkId) throws DataException {
        bookmarkTable.deleteEntity(getBookmark(bookmarkId));
    }
}
