package com.epam.library.businesslayer;

import com.epam.library.businesslayer.interfaces.ILogService;
import com.epam.library.dao.LogAccesses;
import com.epam.library.dao.interfaces.ILogOperations;
import com.epam.library.exception.DataException;
import com.epam.library.model.Log;
import com.epam.library.model.User;
import org.apache.log4j.Logger;

import java.util.List;

public class LogServiceImpl implements ILogService
{
    private ILogOperations logTable = new LogAccesses();
    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");

    @Override
    public List<Log> getAllLogs() throws DataException {
        List<Log> logList = logTable.getAllLogs();
        if (logList.isEmpty()) throw new DataException("Logs are absent");
        return logList;
    }

    @Override
    public void addLog(User user, String message) {
        logTable.addNewEntity(new Log(user.getUserId(),message));
    }
}
