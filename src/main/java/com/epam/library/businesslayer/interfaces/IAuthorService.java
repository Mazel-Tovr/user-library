package com.epam.library.businesslayer.interfaces;

import com.epam.library.exception.DataException;
import com.epam.library.model.Author;

public interface IAuthorService
{
    Author getAuthor(int authorId) throws DataException;
    void addAuthor(String name, String secondName, String lastName, String dob) throws DataException;
    void dropAuthorAndAllHisBooks(int authorId) throws DataException;
    Author getByNameSecondNameLastNameDob(String name, String secondName, String lastName, String dob)throws DataException;
}
