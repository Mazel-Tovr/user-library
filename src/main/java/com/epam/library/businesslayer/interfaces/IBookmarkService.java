package com.epam.library.businesslayer.interfaces;

import com.epam.library.exception.DataException;
import com.epam.library.model.Bookmark;

public interface IBookmarkService
{
    void addBookmark(long userId, long bookId, int pageNumber) throws DataException;
    Bookmark getBookmark(long bookmarkId) throws DataException;
    void dropBookmark(long bookmarkId) throws DataException;

}
