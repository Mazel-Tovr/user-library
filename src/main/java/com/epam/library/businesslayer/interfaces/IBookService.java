package com.epam.library.businesslayer.interfaces;

import com.epam.library.exception.DataException;
import com.epam.library.model.Book;

import java.util.List;

public interface IBookService {
    void addBookToDateBase(String bookName, int releaseYear, int pageCount, String ISBN, String publisher, long authorId) throws DataException;
    Book getBook(long bookId) throws DataException;
    List<Book> getAll() throws DataException;
    void dropBookById(long bookId) throws DataException;
    List<Book> searchBookByPartOfBookName(String partOfName) throws DataException;
    List<Book> searchBookByPartOfAuthorName(String partOfAuthorName) throws DataException;
    Book searchBookByISBN(String ISBN) throws DataException;
    List<Book> searchBookByYearRange(int from,int to) throws DataException;
    List<Book> searchBookByYearAndPageCountAndPartName(int year,int pageCount,String partOfName) throws DataException;
    List<Book> searchBookWhereUserBookMark(long userId) throws DataException;
}
