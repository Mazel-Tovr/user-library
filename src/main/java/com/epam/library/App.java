package com.epam.library;

import com.epam.library.model.User;
import com.epam.library.userinteraction.Entrance;
import com.epam.library.userinteraction.Operation;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class App
{
    private static final Logger logger = Logger.getRootLogger();
    public static void main(String[] args) {


        System.out.println("Welcome to library");
        Entrance entrance = new Entrance();
        Scanner in = new Scanner(System.in);
        User user = null;
        while (user == null) {
            System.out.println("1 - Sign Up\n2 - Sign In");
            String inPut = in.nextLine();
            switch (inPut) {
                case "1":
                    entrance.SignUpAsUser();
                    break;
                case "2":
                    user = entrance.SignIn();
                    break;
                default:
                    System.out.println("Unknown command");
            }

        }
        new Operation(user).userAbility();

    }

}
