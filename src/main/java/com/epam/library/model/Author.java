package com.epam.library.model;

import java.sql.Date;


public class Author extends Entity
{
    private long authorId;

    private String name;

    private String secondName;

    private String lastName;

    private Date dob;


    public Author(long authorId, String name, String secondName, String lastName, Date dob) {
        this.authorId = authorId;
        this.name = name;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dob = dob;
    }

    public Author(String name, String secondName, String lastName, Date dob) {
        this.name = name;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dob = dob;
    }

    public long getAuthorId() {
        return authorId;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getDob() {
        return dob;
    }
}
