package com.epam.library.model;

public class Bookmark extends Entity
{
    private long bookmarkId;

    private long userId;

    private long bookId;

    private int pageNumber;

    public Bookmark(long bookmarkId, long userId, long bookId, int pageNumber) {
        this.bookmarkId = bookmarkId;
        this.userId = userId;
        this.bookId = bookId;
        this.pageNumber = pageNumber;
    }

    public Bookmark(long userId, long bookId, int pageNumber) {
        this.userId = userId;
        this.bookId = bookId;
        this.pageNumber = pageNumber;
    }

    public long getBookmarkId() {
        return bookmarkId;
    }

    public long getUserId() {
        return userId;
    }

    public long getBookId() {
        return bookId;
    }

    public int getPageNumber() {
        return pageNumber;
    }
}
