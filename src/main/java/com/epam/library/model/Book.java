package com.epam.library.model;

public class Book extends Entity
{
    private long bookId;

    private String bookName;

    private int releaseYear;

    private int pageCount;

    private String ISBN;

    private String publisher;

    private Author author; //TODO rewrite!!!


    public Book(long bookId, String bookName, int releaseYear, int pageCount, String ISBN, String publisher, Author author) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.releaseYear = releaseYear;
        this.pageCount = pageCount;
        this.ISBN = ISBN;
        this.publisher = publisher;
        this.author = author;
    }

    public Book(String bookName, int releaseYear, int pageCount, String ISBN, String publisher, Author author) {
        this.bookName = bookName;
        this.releaseYear = releaseYear;
        this.pageCount = pageCount;
        this.ISBN = ISBN;
        this.publisher = publisher;
        this.author = author;
    }

    public long getBookId() {
        return bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String getISBN() {
        return ISBN;
    }

    public String getPublisher() {
        return publisher;
    }

    public Author getAuthor() {
        return author;
    }


    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", bookName='" + bookName +
                ", releaseYear=" + releaseYear +
                ", pageCount=" + pageCount +
                ", ISBN='" + ISBN +
                ", publisher='" + publisher +
                ", authorId=" + author.getAuthorId() +
                '}';
    }
}
