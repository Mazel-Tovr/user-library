package com.epam.library.model;

public class Role extends Entity
{
    private int roleId;

    private String type;

    public Role(int roleId, String type) {
        this.roleId = roleId;
        this.type = type;
    }

    public Role(String type) {
        this.type = type;
    }

    public int getRoleId() {
        return roleId;
    }

    public String getType() {
        return type;
    }
}
