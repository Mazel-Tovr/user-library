package com.epam.library.model;

public class Log extends Entity
{
    private long logId;

    private long userId;

    private String text;

    public Log(long logId, long userId, String text) {
        this.logId = logId;
        this.userId = userId;
        this.text = text;
    }

    public Log(long userId, String text) {
        this.logId = logId;
        this.userId = userId;
        this.text = text;
    }
    public long getLogId() {
        return logId;
    }

    public long getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Log{" +
                "logId=" + logId +
                ", userId=" + userId +
                ", text=" + text +
                " }";
    }
}
