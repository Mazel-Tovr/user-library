package com.epam.library.dao;

import com.epam.library.dao.interfaces.ICommonOperations;
import com.epam.library.model.Bookmark;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BookMarksAccesses implements ICommonOperations<Bookmark> {

    private ConnectionDB dbConnection = ConnectionDB.getInstance();

    private static final Logger logger = Logger.getLogger("DataLayerLogger");

    @Override
    public Bookmark getEntity(long id) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM bookmarks WHERE bookmarkid = ? AND isdeleted = 'False'");
            stmt.setLong(1, id);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new Bookmark(resultSet.getLong("bookmarkid"), resultSet.getLong("userid"),
                        resultSet.getLong("bookid"), resultSet.getInt("pagenumber"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void addNewEntity(Bookmark entity) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("INSERT INTO bookmarks VALUES (Null, ?, ?, ?,'false')");
            stmt.setLong(1, entity.getUserId());
            stmt.setLong(2, entity.getBookId());
            stmt.setInt(3, entity.getPageNumber());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void deleteEntity(Bookmark entity) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("UPDATE bookmarks SET isdeleted = 'True' WHERE bookmarkid = ?");
            stmt.setLong(1,entity.getBookmarkId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void deleteEntity(long id) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("UPDATE bookmarks SET isdeleted = 'True' WHERE bookmarkid = ?");
            stmt.setLong(1,id);
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }
}
