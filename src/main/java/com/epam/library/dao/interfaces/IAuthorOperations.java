package com.epam.library.dao.interfaces;

import com.epam.library.model.Author;
import java.sql.Date;

public interface IAuthorOperations extends ICommonOperations<Author>
{
    void deleteAuthorAndAllHisBooks(int authorId);
    Author isAuthorExist(Author author);
}
