package com.epam.library.dao;

import com.epam.library.ClassConfig;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB
{

    private static final Logger logger = Logger.getLogger("DataLayerLogger");

    private Connection dbConnection = null;

    private static ConnectionDB instance = null;

    private ClassConfig config = ClassConfig.getInstance();

    private ConnectionDB() {
        getDBConnection();
    }

    public static ConnectionDB getInstance()
    {
        if(instance == null)
        {
            synchronized (ConnectionDB.class)
            {
                if(instance == null)
                {
                    instance = new ConnectionDB();
                }
            }
        }
        return instance;
    }

    public Connection getDbConnection() {
        if(dbConnection == null) getDBConnection();
        return dbConnection;
    }

    private void getDBConnection()
    {
        try
        {
            Class.forName(config.getDB_DRIVER());
        }
        catch (ClassNotFoundException e)
        {
            logger.error(e.getMessage());
        }
        try
        {
            dbConnection = DriverManager.getConnection(config.getDB_CONNECTION(),
                    config.getDB_USER(),config.getDB_PASSWORD());
            logger.info("Соединение с базой данных установлено");
            //return true;
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());

        }
        //return false;
    }

}
