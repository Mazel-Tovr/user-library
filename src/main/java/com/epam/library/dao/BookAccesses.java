package com.epam.library.dao;

import com.epam.library.businesslayer.AuthorServiceImpl;
import com.epam.library.businesslayer.interfaces.IAuthorService;
import com.epam.library.dao.interfaces.IAuthorOperations;
import com.epam.library.dao.interfaces.IBookOperations;
import com.epam.library.model.Book;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookAccesses implements IBookOperations {

    private ConnectionDB dbConnection = ConnectionDB.getInstance();

    private static final Logger logger = Logger.getLogger("DataLayerLogger");

    private IAuthorOperations authorOperations = new AuthorAccesses();


    @Override
    public List<Book> getAll() {
        List<Book> bookList = new ArrayList<>();
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM books WHERE isdeleted = 'False'");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                bookList.add(new Book(resultSet.getLong("bookid"), resultSet.getString("bookName"),
                        resultSet.getInt("releaseYear"), resultSet.getInt("pageCount"),
                        resultSet.getString("ISBN"), resultSet.getString("publisher"),
                        authorOperations.getEntity(resultSet.getInt("author"))));
            }

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

        return bookList;
    }

    @Override
    public List<Book> searchBookByPartOfBookName(String partOfName) {
        List<Book> bookList = new ArrayList<>();
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM books WHERE bookName LIKE ? AND isdeleted = 'False'");
            stmt.setString(1, "%" + partOfName + "%");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                bookList.add(new Book(resultSet.getLong("bookid"), resultSet.getString("bookName"),
                        resultSet.getInt("releaseYear"), resultSet.getInt("pageCount"),
                        resultSet.getString("ISBN"), resultSet.getString("publisher"),
                        authorOperations.getEntity(resultSet.getInt("author"))));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return bookList;
    }

    @Override
    public List<Book> searchBookByPartOfAuthorName(String partOfAuthorName) {
        List<Book> bookList = new ArrayList<>();
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM books WHERE author IN " +
                            "(SELECT authorid FROM authors WHERE name LIKE ? AND isdeleted = 'False')" +
                            " AND isdeleted = 'False'");
            stmt.setString(1, "%" + partOfAuthorName + "%");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                bookList.add(new Book(resultSet.getLong("bookid"), resultSet.getString("bookName"),
                        resultSet.getInt("releaseYear"), resultSet.getInt("pageCount"),
                        resultSet.getString("ISBN"), resultSet.getString("publisher"),
                        authorOperations.getEntity(resultSet.getInt("author"))));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return bookList;
    }

    @Override
    public Book searchBookByISBN(String ISBN) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM books WHERE ISBN = ?  AND isdeleted = 'False'");
            stmt.setString(1, ISBN);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new Book(resultSet.getLong("bookid"), resultSet.getString("bookName"),
                        resultSet.getInt("releaseYear"), resultSet.getInt("pageCount"),
                        resultSet.getString("ISBN"), resultSet.getString("publisher"),
                        authorOperations.getEntity(resultSet.getInt("author")));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Book> searchBookByYearRange(int from, int to) {
        List<Book> bookList = new ArrayList<>();
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM books WHERE releaseYear BETWEEN ? AND ? AND isdeleted = 'False'");
            stmt.setInt(1, from);
            stmt.setInt(2, to);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                bookList.add(new Book(resultSet.getLong("bookid"), resultSet.getString("bookName"),
                        resultSet.getInt("releaseYear"), resultSet.getInt("pageCount"),
                        resultSet.getString("ISBN"), resultSet.getString("publisher"),
                        authorOperations.getEntity(resultSet.getInt("author"))));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return bookList;
    }

    @Override
    public List<Book> searchBookByYearAndPageCountAndPartName(int year, int pageCount, String partOfName) {
        List<Book> bookList = new ArrayList<>();
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM books WHERE releaseYear = ? AND pageCount = ? " +
                            "AND bookName LIKE ? AND isdeleted = 'False'");
            stmt.setInt(1, year);
            stmt.setInt(2, pageCount);
            stmt.setString(3, "%" + partOfName + "%");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                bookList.add(new Book(resultSet.getLong("bookid"), resultSet.getString("bookName"),
                        resultSet.getInt("releaseYear"), resultSet.getInt("pageCount"),
                        resultSet.getString("ISBN"), resultSet.getString("publisher"),
                        authorOperations.getEntity(resultSet.getInt("author"))));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return bookList;
    }

    @Override
    public List<Book> searchBookWhereUserBookMark(long userId)
    {
        List<Book> bookList = new ArrayList<>();
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM books WHERE bookid IN " +
                            "(SELECT bookid FROM bookmarks WHERE userid = ? AND isdeleted = 'False' )" +
                            "AND isdeleted = 'False'");

            stmt.setLong(1,userId);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                bookList.add(new Book(resultSet.getLong("bookid"), resultSet.getString("bookName"),
                        resultSet.getInt("releaseYear"), resultSet.getInt("pageCount"),
                        resultSet.getString("ISBN"), resultSet.getString("publisher"),
                        authorOperations.getEntity(resultSet.getInt("author"))));
            }

        }catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return bookList;
    }

    @Override
    public Book getEntity(long id) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM books WHERE bookid = ?  AND isdeleted = 'False'");
            stmt.setLong(1, id);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new Book(resultSet.getLong("bookid"), resultSet.getString("bookName"),
                        resultSet.getInt("releaseYear"), resultSet.getInt("pageCount"),
                        resultSet.getString("ISBN"), resultSet.getString("publisher"),
                        authorOperations.getEntity(resultSet.getInt("author")));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void addNewEntity(Book entity) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection().
                    prepareStatement("INSERT INTO books VALUES (Null, ?, ?, ?, ?, ?, ?,'false')");
            stmt.setString(1, entity.getBookName());
            stmt.setInt(2, entity.getReleaseYear());
            stmt.setInt(3, entity.getPageCount());
            stmt.setString(4, entity.getISBN());
            stmt.setString(5,entity.getPublisher());
            stmt.setLong(6,entity.getAuthor().getAuthorId());
            stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());
        }

    }

    @Override
    public void deleteEntity(Book entity) {

        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement(" UPDATE books SET isdeleted = 'True' WHERE bookid =?");
            stmt.setLong(1,entity.getBookId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void deleteEntity(long id) {

        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement(" UPDATE books SET isdeleted = 'True' WHERE bookid =?");
            stmt.setLong(1,id);
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }
}
