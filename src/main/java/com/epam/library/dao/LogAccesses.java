package com.epam.library.dao;

import com.epam.library.dao.interfaces.ILogOperations;
import com.epam.library.model.Log;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LogAccesses implements ILogOperations
{
    private ConnectionDB dbConnection = ConnectionDB.getInstance();

    private static final Logger logger = Logger.getLogger("DataLayerLogger");

    @Override
    public Log getEntity(long id) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM Logs WHERE logid = ? AND isdeleted = 'False'");
            stmt.setLong(1, id);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next())
            {
                return new Log(resultSet.getLong("logid"),
                        resultSet.getLong("userid"),resultSet.getString("text"));
            }
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void addNewEntity(Log entity)
    {
        try {
        PreparedStatement stmt = dbConnection.getDbConnection()
                .prepareStatement("INSERT INTO Logs VALUES (Null, ?, ?,'false')");
        stmt.setLong(1,entity.getUserId());
        stmt.setString(2,entity.getText());
        stmt.executeUpdate();
        }
        catch (SQLException e)
        {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void deleteEntity(Log entity) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement(" UPDATE Logs SET isdeleted = 'True' WHERE logid =?");
            stmt.setLong(1,entity.getLogId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void deleteEntity(long id) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement(" UPDATE Logs SET isdeleted = 'True' WHERE logid =?");
            stmt.setLong(1,id);
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public List<Log> getAllLogs() {
        List<Log> logList = new ArrayList<>();
        try {

            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM Logs WHERE isdeleted = 'False'");
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                 logList.add(new Log(resultSet.getLong("logid"),
                         resultSet.getLong("userid"),resultSet.getString("text")));
            }

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
     return logList;
    }
}
