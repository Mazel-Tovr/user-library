package com.epam.library.dao;

import com.epam.library.dao.interfaces.IAuthorOperations;
import com.epam.library.model.Author;
import org.apache.log4j.Logger;

import java.sql.*;

public class AuthorAccesses implements IAuthorOperations {

    private ConnectionDB dbConnection = ConnectionDB.getInstance();

    private static final Logger logger = Logger.getLogger("DataLayerLogger");


    @Override
    public Author getEntity(long id) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM authors WHERE authorid = ? AND isdeleted = 'False'");
            stmt.setLong(1, id);
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new Author(resultSet.getInt("authorid"),
                        resultSet.getString("name"), resultSet.getString("secondname"),
                        resultSet.getString("lastname"), resultSet.getDate("dob"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void addNewEntity(Author entity) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection().
                    prepareStatement("INSERT INTO authors VALUES (Null, ?, ?, ?, ?,'false')");
            stmt.setString(1, entity.getName());
            stmt.setString(2, entity.getSecondName());
            stmt.setString(3, entity.getLastName());
            stmt.setDate(4, entity.getDob());//TODO  Test this!!!!!!
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

    }

    @Override
    public void deleteEntity(Author entity) {

        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement(" UPDATE authors SET isdeleted = 'True' WHERE authorid =?");
            stmt.setLong(1, entity.getAuthorId());
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void deleteEntity(long id) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement(" UPDATE authors SET isdeleted = 'True' WHERE authorid =?");
            stmt.setLong(1, id);
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void deleteAuthorAndAllHisBooks(int authorId) {
        try {
//            PreparedStatement stmt = dbConnection.getDbConnection()
//                    .prepareStatement("UPDATE authors SET isdeleted = 'True' WHERE authorid =?");
//            stmt.setLong(1,authorId);
//            stmt.executeUpdate();

            deleteEntity(authorId);

            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("UPDATE books SET isdeleted = 'True' WHERE author =?");
            stmt.setLong(1, authorId);
            stmt.executeUpdate();

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public Author isAuthorExist(Author author) {
        try {
            PreparedStatement stmt = dbConnection.getDbConnection()
                    .prepareStatement("SELECT * FROM authors WHERE name = ? AND secondName = ?" +
                            " AND lastName = ? AND dob = ? AND isdeleted = 'False'");
            stmt.setString(1, author.getName());stmt.setString(2, author.getSecondName());
            stmt.setString(3, author.getLastName());stmt.setDate(4, author.getDob());
            ResultSet resultSet = stmt.executeQuery();
            if (resultSet.next()) {
                return new Author(resultSet.getInt("authorid"),
                        resultSet.getString("name"), resultSet.getString("secondname"),
                        resultSet.getString("lastname"), resultSet.getDate("dob"));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        return null;
    }
}
