package com.epam.library.util.parser;

import com.epam.library.model.Book;

import java.util.List;

public interface IBookParser
{
    List<Book> parseFromFile(String filePath);
}
