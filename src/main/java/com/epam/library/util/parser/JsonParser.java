package com.epam.library.util.parser;

import com.epam.library.model.Book;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;
;

import javax.swing.table.AbstractTableModel;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class JsonParser implements IBookParser {

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");
    @Override
    public List<Book> parseFromFile(String filePath) {

        List<Book> bookList =new ArrayList<>();
        StringBuilder jsonInput = new StringBuilder();
        try {
        Files.lines(Paths.get(filePath), StandardCharsets.UTF_8).forEach(jsonInput::append);

        logger.debug("Date from .csv are successfully gotten");
        GsonBuilder builder = new GsonBuilder().setDateFormat("dd.mm.yyyy");
        Gson gson = builder.create();
       bookList = Arrays.asList(gson.fromJson(jsonInput.toString(), Book[].class));
    } catch (IOException e) {
        logger.info("Wrong file path");
    }
        return bookList;
    }



}
