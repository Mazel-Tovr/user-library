package com.epam.library.util.parser;

import com.epam.library.businesslayer.AuthorServiceImpl;
import com.epam.library.businesslayer.BookServiceImpl;
import com.epam.library.businesslayer.interfaces.IAuthorService;
import com.epam.library.businesslayer.interfaces.IBookService;
import com.epam.library.model.Author;
import com.epam.library.model.Book;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.io.*;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CSVParser implements IBookParser
{

    private static final Logger logger = Logger.getLogger("BusinessLayerLogger");


    @Override
    public List<Book> parseFromFile(String filePath) {
        List<Book> bookList = new ArrayList<>();
        try {
            Reader in = new FileReader(filePath);
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
            for (CSVRecord record : records) {
                if(record.get(0).equals("Book name"))
                    continue;
               bookList.add(
                       new Book(record.get(0),Integer.parseInt(record.get(1)),
                               Integer.parseInt(record.get(2)),record.get(3),record.get(4),
                       new Author(record.get(5),record.get(6),record.get(7),
                               Date.valueOf(new SimpleDateFormat("yyyy-mm-dd")
                                       .format(new SimpleDateFormat("dd.mm.yyyy")
                                               .parse(record.get(8)))))));
            }
            logger.debug("Date from .json are successfully gotten");
           // bookList.forEach(this::addBookToDB);

        }catch (Exception e)
        {
        logger.error(e.getMessage());
        }
        return bookList;
    }


}
